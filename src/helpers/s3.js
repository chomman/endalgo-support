'use strict';

const AWS = require('aws-sdk');

const BUCKET_CODE_BACKUP = 'endalgo-data-backup';

function putObjectToS3(bucket, key, data) {
  const s3 = new AWS.S3();
  const params = {
    Bucket: bucket,
    Key: key,
    Body: data
  };

  s3.putObject(params, function(err, result) {
    if (err) {
      console.log(err, err.stack);
    } else {
      console.log(result);
    }
  });
}

function put(key, data) {
  putObjectToS3(BUCKET_CODE_BACKUP, key, data);
}

module.exports = {
  put,
  putObjectToS3
};

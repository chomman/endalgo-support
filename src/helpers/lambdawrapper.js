'use strict';

const Promise = require('bluebird');
const HTTPStatus = require('http-status');
const LoggedError = require('./loggederror');

/**
 * Simple Lambda Wrapper
 * It provide an abstraction over the lambda callback and Promises runtimes
 */
class LambdaWrapper {
  /**
   * Constructor
   * @param {Function} finalCallback AWS Lambda callback function
   */
  constructor(finalCallback) {
    this.finalCallback = finalCallback;
    this.error = false;
    this.publicResponse = true;
    this.statusCode = HTTPStatus.OK;
    this.body = {
      success: true,
    };
    this.headers = {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    };
    this.p = null;
  }

  /**
   * Run a function wrapped into a Promise
   *
   * @param {Function} func Function to call over a Promise
   *
   * @returns {LambdaWrapper} This instance
   */
  run(func) {
    if (this.p === null) {
      this.p = LambdaWrapper.promisify(func);
    } else {
      this.p = this.p.then(data => LambdaWrapper.promisify(func, data));
    }

    return this;
  }

  /**
   * Build and return an AWS Lambda formated response
   *
   * @returns {Object} AWS Lambda formated response
   */
  getResponse() {
    return {
      headers: this.headers,
      statusCode: this.statusCode,
      // Always stringify Objects inside the 'body'
      body: JSON.stringify(this.body),
    };
  }

  /**
   * Terminate the Lambda process by calling it callback
   * Add the error handler to promise chaining process
   * @param {Function} fn Function to precess result
   *
   * @return {null} Void
   */
  end(fn) {
    this.p.catch(error => {
      console.log('end catch error', error);
      this.error = true;
      this.statusCode = error.status;
      this.body.success = false;
      if (error.isPublic) {
        this.body.error = error.title;
        this.body.error_message = error.message;
        this.body.message = error.publicMessage;
        this.body.defaultMessage = error.defaultMessage;
      } else {
        this.body.error = 'Internal server error';
      }
    }).then(data => {
      console.log('end success data', data);
      // Handle Promise returned data into the final body
      if (data) {
        // Feel free to add custom data modifier here or as the last .run()
        if (fn) {
          this.body= fn(data);
        } else {
          this.body = data;
        }
      }
      this.finalCallback(null, this.getResponse());
    });
  }

  /**
   * Execute a function inside a promise
   *
   * @param {Function} func Function to call over a Promise
   * @param {Object | String} data Datas to pass through the function
   *
   * @return {Promise} Return the Promise itself
   */
  static promisify(func, data) {
    return new Promise(resolve => {
      try {
        resolve(func(data, resolve));
      } catch (error) {
        console.log('error Constructor', Object.getPrototypeOf(error.constructor).name);
        if (Object.getPrototypeOf(error.constructor).name === 'EndalgoError') {
          throw error;
        } else {
          throw LoggedError.createFromError(error);
        }
      }
    });
  }
}

module.exports = LambdaWrapper;

'use strict';

const EndalgoError = require('./endalgoerror');
const HTTPStatus = require('./statuscode');

/**
 * @extends Error
 */
class NotFoundError extends EndalgoError {
  /**
   * Creates a generic error.
   * @param {string} message - Error message.
   * @param {string} publicMessage - public message key for client
   */
  constructor(message, publicMessage) {
    super(message);
    this.name = this.constructor.name;
    this.title = 'Not found exception';
    this.message = message;
    this.status = HTTPStatus.NOT_FOUND;
    this.isPublic = true;
    this.publicMessage = publicMessage;
    this.defaultMessage = message;

    // This is required since bluebird 4 doesn't append it anymore.
    this.isOperational = true;
    console.log('NotFoundError', this);
    Error.captureStackTrace(this, this.constructor.name);
  }
}

module.exports = NotFoundError;

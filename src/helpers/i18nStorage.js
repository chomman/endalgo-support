'use strict';

const dynamodb = require('../utils/dynamodb');
const format = require('string-template');

// i18n table로부터 해당 locale의 코드값 세트를 불러온다.
// 현재 사용하지 않고 있음. 아래 messages 데이터를 선언해 사용하는 것이 빠름
const getI18nCode = (locale) => {
  const params = {
    TableName: process.env.CODE_DB_NAME,
    Key: {
      key: 'i18n',
      type: locale
    },
  };
  console.log('getI18nCode params', params);
  // fetch code from the database
  return dynamodb.get(params).promise();
};

const messages = {
  msg_phone_exist: {
    en: 'You inputed phone number is already exist.',
    ko: '이미 존재하는 전화번호입니다.'
  },
  msg_verify_code: {
    en: 'Verify code: {0}',
    ko: '인증번호: {0}'
  },
  msg_code_sent: {
    en: 'Verification code was sent.',
    ko: '인증번호를 발송했습니다.'
  },
  msg_phone_verified: {
    en: 'Your phone number is verified.',
    ko: '전화번호가 인증되었습니다.'
  },
  msg_code_invalid: {
    en: 'Invalid verification code.',
    ko: '잘못된 인증번호입니다.'
  },
  msg_email_invalid: {
    en: 'Invalid email.',
    ko: '잘못된 이메일입니다.'
  },
  msg_email_exist: {
    en: 'You inputed email is already exist.',
    ko: '이미 존재하는 이메일입니다.'
  },
  msg_email_created: {
    en: 'Email {0} user created.',
    ko: '{0} 이메일 사용자가 생성되었습니다.'
  },
  msg_password_invalid: {
    en: 'Invalid password.',
    ko: '잘못된 비밀번호 형식입니다. 비밀번호는 최소 8자이며 숫자 1개, 특수문자 1개를 포함해야 합니다.'
  },
  msg_baseinfo_saved: {
    en: 'Basic user info was saved.',
    ko: '사용자 기본정보가 저장되었습니다.'
  },
  msg_basic_info_nessesary: {
    en: 'lastName, firstName, password paraters must be set.',
    ko: 'lastName, firstName, password 정보가 반드시 필요합니다.'
  }
};

const getMessage = (locale, key, defaultValue, params) => {
  // console.log(`${key}:${locale} getMessage`);
  let value = messages[key][locale] || defaultValue;
  if (params) {
    // console.log('value before formatting string', value);
    value = format(value, params);
    // console.log('value after formatting string', value);
  }
  return value;
};

module.exports = {
  getI18nCode,
  getMessage
};

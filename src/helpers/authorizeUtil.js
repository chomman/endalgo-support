'use strict';

const _jsonwebtoken = require('jsonwebtoken');
const _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const getUserId = event => {
  let userId = null;
  let authorizationToken = event.headers.authorizationtoken;

  if (authorizationToken) {
    try {
      const data = _jsonwebtoken2.default.verify(authorizationToken, process.env.TOKEN_SECRET, null);
      //console.log('data', data);
      userId = data.id;
    } catch (err) {
      console.error('authorization error', err);
    }
  }

  return userId;
};

exports = module.exports = {
  getUserId
};

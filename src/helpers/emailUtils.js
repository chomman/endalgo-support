'use strict';

const AWS = require('aws-sdk');

const ses = new AWS.SES({
  region: process.env.REGION
});

function validateEmail(email) {
  const regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regex.test(email);
}

function sendEmail(event) {
  console.log('sendEmail parameters', event);
  const params = {
    Destination: {
      ToAddresses: [
        event.to
      ]
    },
    Message: {
      Body: {
        Text: {
          Data: `Email: ${event.from}\nDesc: ${event.description}`,
          Charset: 'UTF-8'
        }
      },
      Subject: {
        Data: event.title,
        Charset: 'UTF-8'
      }
    },
    Source: 'hello@endalgo.com'
  };
  // console.log('ses mail params', params);
  ses.sendEmail(params, function(err, data) {
    if (err) console.log(err);
    else {
      console.log('===EMAIL SENT===');
      console.log(data);
    }
  });
}

function sendHtmlEmail(event) {
  console.log('sendEmail parameters', event);
  const params = {
    Destination: {
      ToAddresses: event.to
    },
    Message: {
      Body: {
        Html: {
          Data: event.description,
          Charset: 'UTF-8'
        }
      },
      Subject: {
        Data: event.title,
        Charset: 'UTF-8'
      }
    },
    Source: 'hello@endalgo.com'
  };
  // console.log('ses mail params', params);
  ses.sendEmail(params, function(err, data) {
    if (err) console.log(err);
    else {
      console.log('===EMAIL SENT===');
      console.log(data);
    }
  });
}

exports = module.exports = {
  validateEmail,
  sendEmail,
  sendHtmlEmail
};

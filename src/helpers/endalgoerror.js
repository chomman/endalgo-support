'use strict';

const HTTPStatus = require('./statuscode');

/**
 * @extends Error
 */
class EndalgoError extends Error {
  /**
   * Creates a generic error.
   * @param {string} message - Error message.
   * @param {string} publicMessage - public message key for client
   */
  constructor(message) {
    super(message);
  }
}

module.exports = EndalgoError;

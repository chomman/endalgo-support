'use strict';

const HTTPStatus = require('./statuscode');
const logger = require('./logger');

/**
 * @extends Error
 */
class ExtendableError extends Error {
  /**
   * Creates a generic error.
   * @param {string} title - Error title.
   * @param {string} message - Error message.
   * @param {number} status - HTTP status code of error.
   * @param {boolean} isPublic - Whether the message
   *                             should be visible to user or not.
   * @param {string} publicMessage - public message key for client
   * @param {string} defaultMessage - default message for client
   */
   constructor(title, message, status, isPublic, publicMessage, defaultMessage) {
     super(message);
    this.name = this.constructor.name;
    this.title = title;
    this.message = message;
    if (!status) {
      this.status = HTTPStatus.INTERNAL_SERVER_ERROR;
    } else {
      this.status = status;
    }
    if (typeof isPublic !== 'boolean') {
      this.isPublic = true;
    } else {
      this.isPublic = isPublic;
      this.publicMessage = publicMessage;
      this.defaultMessage = defaultMessage;
    }
    // This is required since bluebird 4 doesn't append it anymore.
    this.isOperational = true;
    Error.captureStackTrace(this, this.constructor.name);
  }

  /**
   * Create a new instance of this using passed Error Exception
   *
   * @param {Error} error Native JS Error
   * @param {string?} title - Error title.
   * @param {number?} status - HTTP status code of error.
   * @param {boolean?} isPublic - Whether the message
   *                             should be visible to user or not.
   * @param {string?} publicMessage - public message key for client
   * @param {string?} defaultMessage - default message for client
   *
   * @returns {*} A new instance of this (can be ExtendableError or any extended class)
   */
  static createFromError(error, title, status, isPublic, publicMessage, defaultMessage) {
    if (!(error instanceof this)) {
      return new this(
        title ? title : 'Internal server error',
        error.message,
        status ? status : HTTPStatus.INTERNAL_SERVER_ERROR,
        isPublic ? isPublic : false,
        publicMessage,
        defaultMessage
      );
    } else {
      return error;
    }
  }
}

/**
 * Class representing an Logged error.
 * Errors are logged using winston
 *
 * @extends ExtendableError
 */
class LoggedError extends ExtendableError {
  /**
   * Creates an logged error.
   * @param {string} title - Error title.
   * @param {string} message - Error message.
   * @param {number} status - HTTP status code of error.
   * @param {boolean} isPublic - Whether the message
   *                             should be visible to user or not.
   * @param {string} publicMessage - public message key for client
   * @param {string} defaultMessage - default message for client
   */
  constructor(title, message, status, isPublic, publicMessage, defaultMessage) {
    super(title, message, status, isPublic, publicMessage, defaultMessage);
  }
}

module.exports = LoggedError;

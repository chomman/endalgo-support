'use strict';

const uuid = require('uuid');
const Joi = require('joi');
const StringBuilder = require('../../helpers/stringbuilder');
const HTTPStatus = require('../../helpers/statuscode');
const dynamodb = require('../../helpers/dynamodb');
const logger = require('../../helpers/logger');
const LoggedError = require('../../helpers/loggederror');
const NotFoundError = require('../../helpers/notfounderror');
const helpSchema = require('./help.schema');
const helpDao = require('./help.dao');
const emailUtils = require('../../helpers/emailUtils');

class HelpService {
  /**
   * Constructor
   *
   * @param {Object} id Help id object
   */
  constructor(id) {
    if (id) {
      this.id = id;
    }
  }

  setUpdateExpression(specificAttrs) {
    const expression = new StringBuilder('Set ');
    const values = this.update.attributes;

    Object.keys(values).forEach(function(key) {
      const attributeName = specificAttrs[key] || key;
      expression.append(attributeName).append(' = :').append(key).append(',');
    });
    expression.deleteLastChar();

    return expression.toString();
  }

  setUpdateValues(specificAttrs) {
    this.update = {};
    this.update.attributes = {};
    this.update.expressionValues = {};

    const values = this.help.value;
    const $this = this;

    Object.keys(values).forEach(function(key) {
      $this.update.attributes[key] = values[key];
      $this.update.expressionValues[':' + key] = values[key];
    });
    this.update.expression = this.setUpdateExpression(specificAttrs);

    return this.update.expressionValues;
  }

  /**
   * Set Help
   *
   * @param {Object} params Help parameter object
   * @param {Object} specificSchema Joi schema object
   * @param {Object} oldHelp Old help if update
   *
   * @returns {HelpService} this
   */
  setHelp(params, specificSchema, oldHelp) {
    //logger.info('Set params', params);
    const schema = specificSchema || helpSchema();

    this.help = Joi.validate(params, schema);
    logger.log('Validated params by Joi', this);

    if (this.help.error) {
      let statusCode = HTTPStatus.INVALID_PRAMETER;
      let message = 'm.help.edit.parameter.invalid';
      let defaultMessage = 'Invalid parameter.';

      if (this.help.error.details[0].type === 'any.required') {
        statusCode = HTTPStatus.PRAMETER_REQUIRED;
        message = 'm.help.edit.parameter.required';
        defaultMessage = 'The parameter required.';
      }
      throw new LoggedError(
        'Validation Failed',
        this.help.error.message,
        statusCode,
        true,
        message,
        defaultMessage
      );
    }

    if (oldHelp) {
      this.help.value.id = oldHelp.id;
      this.help.value.createdAt = oldHelp.createdAt;
    }

    return this;
  }

  /**
   * Get Help
   *
   * @returns {Object} Help value
   */
  getHelp() {
    logger.debug('this.help', this.help);
    const params = {
      TableName: process.env.HELP_DB_NAME
    };

    if (this.id) {
      logger.info('Get Params', params);
      const added = {
        KeyConditionExpression: 'id = :id',
        ExpressionAttributeValues: {
          ':id': this.id
        }
      };

      return dynamodb.query(Object.assign(params, added), (error, result) => {
        // Handle potential errors
        if (error) {
          console.error(error);
          throw new LoggedError(
            'DynamoDB query Failed',
            error.message,
            HTTPStatus.INTERNAL_SERVER_ERROR,
            false
          );
        }

        logger.info('Get result', result);

        // Create a response
        return result;
      }).promise();
    } else {
      return dynamodb.scan(params, (error, result) => {
        // Handle potential errors
        if (error) {
          console.error(error);
          throw new LoggedError(
            'DynamoDB scan Failed',
            error.message,
            HTTPStatus.INTERNAL_SERVER_ERROR,
            false
          );
        }

        logger.info('Get result', result);

        // Create a response
        return result;
      }).promise();
    }
  }

  /**
   * Transform Help
   * @param {Object} result - dynamodb help request items
   *
   * @returns {Object} object
   */
  transform(result) {
    logger.info('transform param', result);
    if (!result.Items || result.Count === 0) {
      logger.info(1);
      throw new NotFoundError(
        'Help request not found',
        'm.help.id.not_found'
      );
    }

    return result.Items[0];
  }

  /**
   * Save Help
   *
   * @returns {Object} object
   */
  saveHelp() {
    if (this.help) {
      //logger.info('this.help', this.help);
      const newItem = {
        id: this.id || uuid.v1(),
        createdAt: this.help.value.createdAt,
        email: this.help.value.email,
        subject: this.help.value.subject,
        description: this.help.value.description,
        userId: this.help.value.userId,
        userAgent: this.help.value.userAgent,
        ip: this.help.value.ip
      };
      return helpDao.saveHelp(newItem);
    } else {
      throw new LoggedError(
        'Add Help Failed',
        'Parametes required.',
        HTTPStatus.PRAMETER_REQUIRED,
        true
      );
    }
  }

  sendEmail(saved) {
    emailUtils.sendHtmlEmail({
      from: saved.email,
      to: ['help@endalgo.com', 'taehyun@endalgo.com'],
      title: `[Help Request] ${saved.subject}`,
      description: `<p>Help request from
          <br>email : ${saved.email}
          <br>userId : ${saved.userId}
          <br>userAgent : ${saved.userAgent}
          <br>ip : ${saved.ip}
        </p><hr><br>
        <p>${saved.description}</p>`
    });
  }

  /**
   * Update Help
   *
   * @returns {Object} object
   */
  updateHelp() {
    this.setUpdateValues({
      description: '#help_text'
    });

    logger.info('this.help', this.help);
    const params = {
      TableName: process.env.HELP_DB_NAME,
      Key: {
        id: this.id,
      },
      ExpressionAttributeNames: {
        '#help_text': 'description',
      },
      ExpressionAttributeValues: this.update.expressionValues,
      UpdateExpression: this.update.expression,
      ReturnValues: 'UPDATED_NEW',
    };

    logger.info('Create Params', params);

    return dynamodb.update(params, (error, result) => {
      // Handle potential errors
      if (error) {
        console.error(error);
        throw new LoggedError(
          'DynamoDB update Failed',
          error.message,
          HTTPStatus.INTERNAL_SERVER_ERROR,
          false
        );
      }

      logger.info('Update result', result);

      // Create a response
      return result;
    }).promise();
  }

  /**
   * Delete Help
   *
   * @param {Object} id Help object
   *
   * @returns {Object} object
   */
  deleteHelp(help) {
    logger.info('delete id', this.id);
    const params = {
      TableName: process.env.HELP_DB_NAME,
      Key: {
        id: this.id || help.id,
        createdAt: help.createdAt
      },
    };

    logger.info('Create Params', params);

    return dynamodb.delete(params, (error, result) => {
      // Handle potential errors
      if (error) {
        console.error(error);
        throw new LoggedError(
          'DynamoDB delete Failed',
          error.message,
          HTTPStatus.INTERNAL_SERVER_ERROR,
          false
        );
      }

      logger.info('Delete result', result);

      // Create a response
      return result;
    }).promise();
  }
}

module.exports = HelpService;

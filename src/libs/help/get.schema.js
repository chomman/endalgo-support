'use strict';

const Joi = require('joi');

const schema = {
  id: Joi.string().required()
};

module.exports = schema;

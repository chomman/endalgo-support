'use strict';

const dynamodb = require('../../helpers/dynamodb');
const logger = require('../../helpers/logger');
const LoggedError = require('../../helpers/loggederror');

const table = process.env.HELP_DB_NAME;
const config = {
  region: process.env.REGION
};

if (process.env.LOCAL_DDB_ENDPOINT) config.endpoint = process.env.LOCAL_DDB_ENDPOINT;

const saveHelp = help => new Promise((resolve, reject) => {
  //logger.info('save help', help);

  const putHelp = () => {
    const params = {
      TableName: table,
      Item: help
    };

    logger.info('Create Params', params);

    return dynamodb.put(params).promise();
  };

  putHelp()
    .then(result => resolve(help))
    .catch(reject);
});

exports = module.exports = {
  saveHelp
};

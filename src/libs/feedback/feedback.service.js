'use strict';

const uuid = require('uuid');
const Joi = require('joi');
const StringBuilder = require('../../helpers/stringbuilder');
const HTTPStatus = require('../../helpers/statuscode');
const dynamodb = require('../../helpers/dynamodb');
const logger = require('../../helpers/logger');
const LoggedError = require('../../helpers/loggederror');
const NotFoundError = require('../../helpers/notfounderror');
const feedbackSchema = require('./feedback.schema');
const feedbackDao = require('./feedback.dao');
const emailUtils = require('../../helpers/emailUtils');

class FeedbackService {
  /**
   * Constructor
   *
   * @param {Object} id Feedback id object
   */
  constructor(id) {
    if (id) {
      this.id = id;
    }
  }

  setUpdateExpression(specificAttrs) {
    const expression = new StringBuilder('Set ');
    const values = this.update.attributes;

    Object.keys(values).forEach(function(key) {
      const attributeName = specificAttrs[key] || key;
      expression.append(attributeName).append(' = :').append(key).append(',');
    });
    expression.deleteLastChar();

    return expression.toString();
  }

  setUpdateValues(specificAttrs) {
    this.update = {};
    this.update.attributes = {};
    this.update.expressionValues = {};

    const values = this.feedback.value;
    const $this = this;

    Object.keys(values).forEach(function(key) {
      $this.update.attributes[key] = values[key];
      $this.update.expressionValues[':' + key] = values[key];
    });
    this.update.expression = this.setUpdateExpression(specificAttrs);

    return this.update.expressionValues;
  }

  /**
   * Set Feedback
   *
   * @param {Object} params Feedback parameter object
   * @param {Object} specificSchema Joi schema object
   *
   * @returns {FeedbackService} this
   */
  setFeedback(params, specificSchema) {
    //logger.info('Set params', params);
    const schema = specificSchema || feedbackSchema();

    this.feedback = Joi.validate(params, schema);
    //logger.info('Validated params by Joi', this);

    if (this.feedback.error) {
      let statusCode = HTTPStatus.INVALID_PRAMETER;
      let message = 'm.feedback.edit.paramter.invalid';
      let defaultMessage = 'Invalid parameter.';

      if (this.feedback.error.details[0].type === 'any.required') {
        statusCode = HTTPStatus.PRAMETER_REQUIRED;
        message = 'm.feedback.edit.parameter.required';
        defaultMessage = 'The parameter required.';
      }
      throw new LoggedError(
        'Validation Failed',
        this.feedback.error.message,
        statusCode,
        true,
        message,
        defaultMessage
      );
    }

    return this;
  }

  /**
   * Get Feedback
   *
   * @returns {Object} Feedback value
   */
  getFeedback() {
    logger.info('this.feedback', this.feedback);
    const params = {
      TableName: process.env.FEEDBACK_DB_NAME,
      Key: {
        id: this.id
      }
    };

    logger.info('Get Params', params);

    return dynamodb.get(params, (error, result) => {
      // Handle potential errors
      if (error) {
        console.error(error);
        throw new LoggedError(
          'DynamoDB get Failed',
          error.message,
          HTTPStatus.INTERNAL_SERVER_ERROR,
          false
        );
      }

      logger.info('Get result', result);

      // Create a response
      return result;
    }).promise();
  }

  /**
   * Get Feedback
   *
   * @returns {Object} Feedback value
   */
  getFeedbackList() {
    logger.debug('this.feedback', this.feedback);
    const params = {
      TableName: process.env.FEEDBACK_DB_NAME
    };

    if (this.id) {
      params.Key = {
        id: this.id
      };
    }

    logger.info('feedback', this.feedback);

    if (this.feedback && this.feedback.value.email) {
      params.FilterExpression = '#email = :email';
      params.ExpressionAttributeNames = {
        '#email': 'email'
      };
      params.ExpressionAttributeValues = {
        ':email': this.feedback.value.email
      };
    }

    if (this.feedback && this.feedback.value.createdAt) {
      if (params.KeyConditionExpression) {
        params.FilterExpression = `${params.KeyConditionExpression} and createdAt > :createdAt`;
      } else {
        params.FilterExpression = 'createdAt > :createdAt';
      }

      params.ExpressionAttributeValues = {
        ':createdAt': this.feedback.value.createdAt
      };
    }

    logger.info('Get Params', params);

    return dynamodb.scan(params, (error, result) => {
      // Handle potential errors
      if (error) {
        console.error(error);
        throw new LoggedError(
          'DynamoDB put Failed',
          error.message,
          HTTPStatus.INTERNAL_SERVER_ERROR,
          false
        );
      }

      logger.info('Get result', result);

      // Create a response
      return result;
    }).promise();
  }

  /**
   * Save Feedback
   *
   * @returns {Object} object
   */
  saveFeedback() {
    if (this.feedback) {
      const newItem = {
        id: this.id || uuid.v1(),
        createdAt: this.feedback.value.createdAt,
        email: this.feedback.value.email,
        subject: this.feedback.value.subject,
        description: this.feedback.value.description,
        userId: this.feedback.value.userId,
        userAgent: this.feedback.value.userAgent,
        ip: this.feedback.value.ip
      };
      return feedbackDao.saveFeedback(newItem);
    } else {
      throw new LoggedError(
        'Add Feedback Failed',
        'Parametes required.',
        HTTPStatus.PRAMETER_REQUIRED,
        true
      );
    }
  }

  sendEmail(saved) {
    emailUtils.sendHtmlEmail({
      from: saved.email,
      to: ['feedback@endalgo.com', 'taehyun@endalgo.com'],
      title: `[Feeackdb Request] ${saved.subject}`,
      description: `<p>Feedback request from
          <br>email : ${saved.email}
          <br>userId : ${saved.userId}
          <br>userAgent : ${saved.userAgent}
          <br>ip : ${saved.ip}
        </p><hr><br>
        <p>${saved.description}</p>`
    });
  }

  /**
   * Update Feedback
   *
   * @returns {Object} object
   */
  updateFeedback() {
    this.setUpdateValues({
      description: '#feedback_text'
    });

    logger.info('this.feedback', this.feedback);
    const params = {
      TableName: process.env.FEEDBACK_DB_NAME,
      Key: {
        id: this.id,
      },
      ExpressionAttributeNames: {
        '#feedback_text': 'description',
      },
      ExpressionAttributeValues: this.update.expressionValues,
      UpdateExpression: this.update.expression,
      ReturnValues: 'UPDATED_NEW',
    };

    logger.info('Create Params', params);

    return dynamodb.update(params, (error, result) => {
      // Handle potential errors
      if (error) {
        console.error(error);
        throw new LoggedError(
          'DynamoDB update Failed',
          error.message,
          HTTPStatus.INTERNAL_SERVER_ERROR,
          false
        );
      }

      logger.info('Update result', result);

      // Create a response
      return result;
    }).promise();
  }

  /**
   * Delete Feedback
   *
   * @param {Object} id Feedback object
   *
   * @returns {Object} object
   */
  deleteFeedback() {
    logger.debug('delete id', this.id);
    const params = {
      TableName: process.env.FEEDBACK_DB_NAME,
      Key: {
        id: this.id
      },
    };

    logger.debug('Create Params', params);

    return dynamodb.delete(params, (error, result) => {
      // Handle potential errors
      if (error) {
        console.error(error);
        throw new LoggedError(
          'DynamoDB delete Failed',
          error.message,
          HTTPStatus.INTERNAL_SERVER_ERROR,
          false
        );
      }

      logger.info('Delete result', result);

      // Create a response
      return result;
    }).promise();
  }
}

module.exports = FeedbackService;

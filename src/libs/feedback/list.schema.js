'use strict';

const Joi = require('joi');

const schema = {
  email: Joi.string().email(),
  createdAt: Joi.date().timestamp('javascript').default(1262304000),
};

module.exports = schema;

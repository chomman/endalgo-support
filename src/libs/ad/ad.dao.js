'use strict';

const logger = require('../../helpers/logger');
const LoggedError = require('../../helpers/loggederror');

const table = process.env.AD_DB_NAME;
const config = {
  region: process.env.REGION
};

if (process.env.LOCAL_DDB_ENDPOINT) config.endpoint = process.env.LOCAL_DDB_ENDPOINT;

// Common
const AWS = require('aws-sdk');
const Promise = require('bluebird');

const dynamodb = new AWS.DynamoDB.DocumentClient(config);

const saveAd = ad => new Promise((resolve, reject) => {
  //logger.info('save ad', ad);

  const putAd = () => {
    const params = {
      TableName: table,
      Item: ad
    };

    logger.info('Create Params', params);

    return dynamodb.put(params).promise();
  };

  putAd()
    .then(result => resolve(ad))
    .catch(reject);
});

exports = module.exports = {
  saveAd
};

'use strict';

const Joi = require('joi');

const schema = () => {
  const now = new Date().getTime();

  const s = Joi.object({
    email: Joi.string().email().required(),
    createdAt: Joi.date().timestamp('javascript').default(now),
    subject: Joi.string().required(),
    description: Joi.string().required(),
    userId: Joi.string().allow(null),
    ip: Joi.string().allow(null),
    userAgent: Joi.string().allow(null)
  });

  return s;
};

module.exports = schema;

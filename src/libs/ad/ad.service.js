'use strict';

const uuid = require('uuid');
const Joi = require('joi');
const StringBuilder = require('../../helpers/stringbuilder');
const HTTPStatus = require('../../helpers/statuscode');
const dynamodb = require('../../helpers/dynamodb');
const logger = require('../../helpers/logger');
const LoggedError = require('../../helpers/loggederror');
const adSchema = require('./ad.schema');
const adDao = require('./ad.dao');
const emailUtils = require('../../helpers/emailUtils');

class AdService {
  /**
   * Constructor
   *
   * @param {Object} id Ad id object
   */
  constructor(id) {
    if (id) {
      this.id = id;
    }
  }

  setUpdateExpression(specificAttrs) {
    const expression = new StringBuilder('Set ');
    const values = this.update.attributes;

    Object.keys(values).forEach(function(key) {
      const attributeName = specificAttrs[key] || key;
      expression.append(attributeName).append(' = :').append(key).append(',');
    });
    expression.deleteLastChar();

    return expression.toString();
  }

  setUpdateValues(specificAttrs) {
    this.update = {};
    this.update.attributes = {};
    this.update.expressionValues = {};

    const values = this.ad.value;
    const $this = this;

    Object.keys(values).forEach(function(key) {
      $this.update.attributes[key] = values[key];
      $this.update.expressionValues[':' + key] = values[key];
    });
    this.update.expression = this.setUpdateExpression(specificAttrs);

    return this.update.expressionValues;
  }

  /**
   * Set Ad
   *
   * @param {Object} params Ad parameter object
   * @param {Object} specificSchema Joi schema object
   *
   * @returns {AdService} this
   */
  setAd(params, specificSchema) {
    //logger.info('Set params', params);
    const schema = specificSchema || adSchema();

    this.ad = Joi.validate(params, schema);
    logger.info('Validated params by Joi', this);

    if (this.ad.error) {
      let statusCode = HTTPStatus.INVALID_PRAMETER;
      let message = 'm.ad.edit.paramter.invalid';
      let defaultMessage = 'Invalid parameter.';

      if (this.ad.error.details[0].type === 'any.required') {
        statusCode = HTTPStatus.PRAMETER_REQUIRED;
        message = 'm.ad.edit.parameter.required';
        defaultMessage = 'The parameter required.';
      }
      throw new LoggedError(
        'Validation Failed',
        this.ad.error.message,
        statusCode,
        true,
        message,
        defaultMessage
      );
    }

    return this;
  }

  /**
   * Get Ad
   *
   * @returns {Object} Ad value
   */
  getAd() {
    logger.info('this.ad', this.ad);
    const params = {
      TableName: process.env.AD_DB_NAME,
      Key: {
        id: this.id
      }
    };

    logger.info('Get Params', params);

    return dynamodb.get(params, (error, result) => {
      // Handle potential errors
      if (error) {
        console.error(error);
        throw new LoggedError(
          'DynamoDB get Failed',
          error.message,
          HTTPStatus.INTERNAL_SERVER_ERROR,
          false
        );
      }

      logger.info('Get result', result);

      // Create a response
      return result;
    }).promise();
  }

  /**
   * Save Ad
   *
   * @returns {Object} object
   */
  saveAd() {
    if (this.ad) {
      //logger.info('this.ad', this.ad);
      const newItem = {
        id: this.id || uuid.v1(),
        createdAt: this.ad.value.createdAt,
        email: this.ad.value.email,
        subject: this.ad.value.subject,
        description: this.ad.value.description,
        userId: this.ad.value.userId,
        userAgent: this.ad.value.userAgent,
        ip: this.ad.value.ip
      };
      return adDao.saveAd(newItem);
    } else {
      throw new LoggedError(
        'Add Ad Failed',
        'Parametes required.',
        HTTPStatus.PRAMETER_REQUIRED,
        true
      );
    }
  }

  sendEmail(saved) {
    emailUtils.sendHtmlEmail({
      from: saved.email,
      to: ['ads@endalgo.com', 'taehyun@endalgo.com'],
      title: `[Ads Request] ${saved.subject}`,
      description: `<p>Ads request from
          <br>email : ${saved.email}
          <br>userId : ${saved.userId}
          <br>userAgent : ${saved.userAgent}
          <br>ip : ${saved.ip}
        </p><hr><br>
        <p>${saved.description}</p>`
    });
  }

  /**
   * Get Feedback
   *
   * @returns {Object} Feedback value
   */
  getAdList() {
    logger.debug('this.ad', this.ad);
    const params = {
      TableName: process.env.AD_DB_NAME
    };

    if (this.id) {
      params.Key = {
        id: this.id
      };
    }

    logger.info('ad', this.ad);

    if (this.ad && this.ad.value.email) {
      params.FilterExpression = '#email = :email';
      params.ExpressionAttributeNames = {
        '#email': 'email'
      };
      params.ExpressionAttributeValues = {
        ':email': this.ad.value.email
      };
    }

    if (this.ad && this.ad.value.createdAt) {
      if (params.KeyConditionExpression) {
        params.FilterExpression = `${params.KeyConditionExpression} and createdAt > :createdAt`;
      } else {
        params.FilterExpression = 'createdAt > :createdAt';
      }

      params.ExpressionAttributeValues = {
        ':createdAt': this.ad.value.createdAt
      };
    }

    logger.info('Get Params', params);

    return dynamodb.scan(params, (error, result) => {
      // Handle potential errors
      if (error) {
        console.error(error);
        throw new LoggedError(
          'DynamoDB put Failed',
          error.message,
          HTTPStatus.INTERNAL_SERVER_ERROR,
          false
        );
      }

      logger.info('Get result', result);

      // Create a response
      return result;
    }).promise();
  }

  /**
   * Update Ad
   *
   * @returns {Object} object
   */
  updateAd() {
    this.setUpdateValues({
      description: '#ad_text'
    });

    logger.info('this.ad', this.ad);
    const params = {
      TableName: process.env.AD_DB_NAME,
      Key: {
        id: this.id,
      },
      ExpressionAttributeNames: {
        '#ad_text': 'description',
      },
      ExpressionAttributeValues: this.update.expressionValues,
      UpdateExpression: this.update.expression,
      ReturnValues: 'UPDATED_NEW',
    };

    logger.info('Create Params', params);

    return dynamodb.update(params, (error, result) => {
      // Handle potential errors
      if (error) {
        console.error(error);
        throw new LoggedError(
          'DynamoDB update Failed',
          error.message,
          HTTPStatus.INTERNAL_SERVER_ERROR,
          false
        );
      }

      logger.info('Update result', result);

      // Create a response
      return result;
    }).promise();
  }

  /**
   * Delete Ad
   *
   * @param {Object} id Ad object
   *
   * @returns {Object} object
   */
  deleteAd() {
    logger.info('delete id', this.id);
    const params = {
      TableName: process.env.AD_DB_NAME,
      Key: {
        id: this.id
      },
    };

    logger.info('Create Params', params);

    return dynamodb.delete(params, (error, result) => {
      // Handle potential errors
      if (error) {
        console.error(error);
        throw new LoggedError(
          'DynamoDB delete Failed',
          error.message,
          HTTPStatus.INTERNAL_SERVER_ERROR,
          false
        );
      }

      logger.info('Delete result', result);

      // Create a response
      return result;
    }).promise();
  }
}

module.exports = AdService;

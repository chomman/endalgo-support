'use strict';

const debug = require('debug')('API');
const logger = require('../../helpers/logger');
const LambdaWrapper = require('../../helpers/lambdawrapper');
const FeedbackService = require('../../libs/feedback/feedback.service');

debug('API Started');

module.exports.run = (event, context, callback) => {
  logger.info('Running API', { pid: process.pid });

  // Set parametes from client
  const id = event.pathParameters.id;

  const app = new LambdaWrapper(callback);
  const service = new FeedbackService(id);

  app.run(() => service.getFeedback())
     .end();
};

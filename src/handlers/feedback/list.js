'use strict';

const debug = require('debug')('API');
const logger = require('../../helpers/logger');
const LambdaWrapper = require('../../helpers/lambdawrapper');
const FeedbackService = require('../../libs/feedback/feedback.service');
const listSchema = require('../../libs/feedback/list.schema');

debug('API Started');

module.exports.run = (event, context, callback) => {
  logger.info('Running API', { pid: process.pid });

  // Set parametes from client
  const params = JSON.parse(event.body) || {};

  const app = new LambdaWrapper(callback);
  const service = new FeedbackService();

  app.run(() => service.setFeedback(params, listSchema))
     .run(() => service.getFeedbackList())
     .end();
};

'use strict';

const debug = require('debug')('API');
const logger = require('../../helpers/logger');
const LambdaWrapper = require('../../helpers/lambdawrapper');
const HelpService = require('../../libs/help/help.service');

debug('API Started');

module.exports.run = (event, context, callback) => {
  logger.info('Running API', { pid: process.pid });

  // Set parametes from client
  const id = event.pathParameters.id;
  const params = JSON.parse(event.body);

  const app = new LambdaWrapper(callback);
  const service = new HelpService(id);

  app.run(() => service.getHelp())
     .run(result => service.transform(result))
     .run(result => service.setHelp(params, null, result))
     .run(() => service.saveHelp())
     .end();
};

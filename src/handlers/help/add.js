'use strict';

const debug = require('debug')('API');
const logger = require('../../helpers/logger');
const LambdaWrapper = require('../../helpers/lambdawrapper');
const HelpService = require('../../libs/help/help.service');
const authorizeUtil = require('../../helpers/authorizeUtil');

debug('API Started');

module.exports.run = (event, context, callback) => {
  logger.info('Running API', {
    pid: process.pid
  });

  // Set parametes from client
  const params = JSON.parse(event.body);
  const userId = authorizeUtil.getUserId(event);

  params.userId = userId;
  params.ip = event.requestContext.identity.sourceIp;
  params.userAgent = event.requestContext.identity.userAgent;

  const app = new LambdaWrapper(callback);
  const service = new HelpService();

  app.run(() => service.setHelp(params))
    .run(() => service.saveHelp())
    .run(saved => service.sendEmail(saved))
    .end();
};

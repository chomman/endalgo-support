'use strict';

const debug = require('debug')('API');
const logger = require('../../helpers/logger');
const LambdaWrapper = require('../../helpers/lambdawrapper');
const HelpService = require('../../libs/help/help.service');
const listSchema = require('../../libs/help/list.schema');

debug('API Started');

module.exports.run = (event, context, callback) => {
  logger.info('Running API', { pid: process.pid });

  // Set parametes from client
  const params = JSON.parse(event.body) || {};

  const app = new LambdaWrapper(callback);
  const service = new HelpService();

  app.run(() => service.setHelp(params, listSchema))
     .run(() => service.getHelp())
     .end();
};

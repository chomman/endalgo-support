'use strict';

const debug = require('debug')('API');
const logger = require('../../helpers/logger');
const LambdaWrapper = require('../../helpers/lambdawrapper');
const AdService = require('../../libs/ad/ad.service');

debug('API Started');

module.exports.run = (event, context, callback) => {
  logger.info('Running API', { pid: process.pid });

  // Set parametes from client
  const id = event.pathParameters.id;

  const app = new LambdaWrapper(callback);
  const service = new AdService(id);

  app.run(() => service.deleteAd())
     .end();
};

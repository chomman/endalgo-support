'use strict';

const debug = require('debug')('API');
const logger = require('../../helpers/logger');
const LambdaWrapper = require('../../helpers/lambdawrapper');
const AdService = require('../../libs/ad/ad.service');
const listSchema = require('../../libs/feedback/list.schema');

debug('API Started');

module.exports.run = (event, context, callback) => {
  logger.info('Running API', { pid: process.pid });

  // Set parametes from client
  const params = JSON.parse(event.body) || {};

  const app = new LambdaWrapper(callback);
  const service = new AdService();

  app.run(() => service.setAd(params, listSchema))
     .run(() => service.getAdList())
     .end();
};

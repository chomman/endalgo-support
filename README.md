endalgo-help
============

ENDALGO Help Center, Feedback, Ad 관련 CRUD API입니다.

사실상 3개가 모두 내용이 동일하나 이후 다른 형태가 될 가능성이 있어 분기해 놓았습니다.

[![ENDALGO Sangdo](https://img.shields.io/badge/ENDALGO-sangdo-red.svg)](https://bitbucket.org/endalgoserver/generator-endalgo-serverless-sangdo)

Creating AWS Access Keys
------------------------

1.	Create or login to your Amazon Web Services Account and go to the Identity & Access Management (IAM) page.

2.	Click on Users and then Add user. Enter a name in the first field to remind you this User is the Framework, like serverless-admin. Enable Programmatic access by clicking the checkbox. Click Next to go through to the Permissions page. Click on Attach existing policies directly. Search for and select AdministratorAccess then click Next: Review. Check everything looks good and click Create user. Later, you can create different IAM Users for different apps and different stages of those apps. That is, if you don't use separate AWS accounts for stages/apps, which is most common.

3.	View and copy the API Key & Secret to a temporary place. You'll need it in the next step.

Using AWS Access Keys
---------------------

You can configure the Serverless Framework to use your **AWS API Key** & **Secret** in two ways:

### Quick Setup

As a quick setup to get started you can export them as environment variables so they would be accessible to Serverless and the AWS SDK in your shell:

\`\``export AWS_ACCESS_KEY_ID=<your-key-here> export AWS_SECRET_ACCESS_KEY=<your-secret-key-here>

```
### Using AWS Profiles

#### Setup with `serverless config credentials` command

Serverless provides a convenient way to configure AWS profiles with the help of the `serverless config credentials` command.

Here's an example how you can configure the default AWS profile:

```

serverless config credentials --provider aws --key AKIAIOSFODNN7EXAMPLE --secret wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY

```
Take a look at the config CLI reference for more information about credential configuration.

#### Setup with the `aws-cli`

To set them up through the aws-cli install it first then run aws configure to configure the aws-cli and credentials:
```

$ aws configure AWS Access Key ID [None]: AKIAIOSFODNN7EXAMPLE AWS Secret Access Key [None]: wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY Default region name [None]: us-west-2 Default output format [None]: ENTER

```
Credentials are stored in INI format in ~/.aws/credentials, which you can edit directly if needed. You can change the path to the credentials file via the AWS_SHARED_CREDENTIALS_FILE environment variable. Read more about that file in the AWS documentation

You can even set up different profiles for different accounts, which can be used by Serverless as well. To specify a default profile to use, you can add a profile setting to your provider configuration in `serverless.yml`:
```

service: new-service provider: name: aws runtime: nodejs6.10 stage: dev profile: devProfile

```
#### Use an existing AWS Profile

To easily switch between projects without the need to do `aws configure` every time you can use environment variables. For example you define different profiles in `~/.aws/credentials`

```[profileName1]
aws_access_key_id=***************
aws_secret_access_key=***************

[profileName2]
aws_access_key_id=***************
aws_secret_access_key=***************
```

Now you can switch per project (/ API) by executing once when you start your project:

`export AWS_PROFILE="profileName2" && export AWS_REGION=eu-west-1.`

in the Terminal. Now everything is set to execute all the serverless CLI options like `sls deploy`. The AWS region setting is to prevent issues with specific services, so adapt if you need another default region.

#### Per Stage Profiles

As an advanced use-case, you can deploy different stages to different accounts by using different profiles per stage. In order to use different profiles per stage, you must leverage variables and the provider profile setting.

This example `serverless.yml` snippet will load the profile depending upon the stage specified in the command line options (or default to 'dev' if unspecified);

```
service: new-service
provider:
  name: aws
  runtime: nodejs6.10
  stage: ${opt:stage, self:custom.defaultStage}
  profile: ${self:custom.profiles.${self:provider.stage}}
custom:
  defaultStage: dev
  profiles:
    dev: devProfile
    prod: prodProfile
```

Command Line Interface
----------------------

### Create `env.json` file

```bash
    $ cp config/env.dev.json config/env.json
    # or
    $ cp config/env.prod.json config/env.json
```

Then edit `config/env.json`.

### Running locally

Uncomment the plugin section in `serverless.yml` then:

```bash
    $ sls offline
```

### Deploy project remotely

```bash
    $ bash bin/deploy.sh
```

### Deploy a function remotely

```bash
    $ sls deploy function -f api --region us-east-1
```

Continuous Integration
----------------------

### Creating tests

```bash
    $ sls create test -f functionName
```

More information on [serverless-mocha-plugin](https://github.com/SC5/serverless-mocha-plugin)

### Test with mocha

```bash
    $ npm test
```

### Run full CI Stack

```bash
    $ npm run -s ci
```

### Run Eslint

```bash
    $ npm run -s lint
```

### Run dependencies security check

```bash
    $ npm run -s security
```

Miscellaneous
-------------

### Run Unit Test With Nyan Cat

```bash
  $ npm run -s test-nyan
```
